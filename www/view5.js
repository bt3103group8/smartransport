jQuery(document).ready(function($) {
	var oAP_min;
	var sStartD;
	var sStartD_moment, sEndD_moment;
	var MAPID = 'view5_heatmapUI-mapmap';
	var formatDate = "dd MMM yyyy";
	var formatDate_moment = "DD MMM YYYY";
	var formatDateTime_moment = "hh:mm A - DD MMM YYYY";

	sStartD = new Date();
	sStartD.setHours(0,0,0,0);

	$("#ip-start-date").AnyPicker(
	{
		mode: "datetime",

		inputDateTimeFormat: formatDate,
		dateTimeFormat: formatDate,

		onInit: function()
		{
			oAP_min = this;
			oAP_min.setSelectedDate(sStartD);
			oAP_min.setMaximumDate(sStartD);
			var setHeatmapTimefrom = setInterval(function() {
				if(typeof Shiny !== 'undefined') {
					sStartD_moment = moment(sStartD).format(formatDateTime_moment);
					sEndD_moment = moment(sStartD).endOf('hour').format(formatDateTime_moment);
					Shiny.onInputChange("heatmap_timefrom", sStartD_moment);
					Shiny.onInputChange("heatmap_timeto", sEndD_moment);
					clearInterval(setHeatmapTimefrom);
				}
			},100);
		},

		onSetOutput: function(sOutput, oSelectedValues)
		{
			sStartD = sOutput;
			sStartD_moment = moment(sStartD, formatDate_moment).format(formatDateTime_moment);
			sEndD_moment = moment(sStartD, formatDate_moment).endOf('hour').format(formatDateTime_moment);
			Shiny.onInputChange("heatmap_timefrom", sStartD_moment);
			Shiny.onInputChange("heatmap_timeto", sEndD_moment);
			Shiny.onInputChange("pingResetHeatmapSlider", Math.random());
		}
	});

	$("#ip-start-date").click(function(event) {
		oAP_min.showOrHidePicker(sStartD);
	});

	var forceDisableZoom = setInterval(function() {
		if(typeof window[MAPID] !== 'undefined') {
			window[MAPID].setOptions({minZoom: 11, maxZoom: 11});
		}
	}, 100);

/*
	$("#ip-end-date").AnyPicker(
	{
		mode: "datetime",

		inputDateTimeFormat: formatDateTime,
		dateTimeFormat: formatDateTime,

		onInit: function()
		{
			oAP_max = this;
			oAP_max.setMinimumDate(sStartD);
			oAP_max.setSelectedDate(sEndD);
			var setHeatmapTimeto = setInterval(function() {
				if(typeof Shiny !== 'undefined') {
					Shiny.onInputChange("heatmap_timeto", oAP_max.formatOutputDates(sEndD));
					clearInterval(setHeatmapTimeto);
				}
			}, 100);
		},

		onSetOutput: function(sOutput, oSelectedValues)
		{
			sEndD = sOutput;
			oAP_min.setMaximumDate(sEndD);
			Shiny.onInputChange("heatmap_timeto", sEndD);
			setTimeout(function() {
				window[MAPID].setZoom(11);
			}, 500);
		}
	});

	$("#ip-end-date").click(function(event) {
		oAP_max.showOrHidePicker(sEndD);
	});
*/

	$('body').on('change', '#location-sharing-checkbox', function(event) {
		if(event.target.checked) {
			$('#location-sharing-span').html("Location sharing enabled");
			Shiny.onInputChange('location_to_firebase', true);
		}
		else {
			$('#location-sharing-span').html("Share your location?");
			Shiny.onInputChange('location_to_firebase', false);
		}
	});

});
