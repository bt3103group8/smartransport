function view2_formatDataTable(table) {
	return table;
}

jQuery(document).ready(function($) {
	$('body').on('click', '#view2_mrtBreakdownTableUI-mrt_breakdown tbody tr', function(event) {
		$('.page-content').scrollTop(0, 500);
	});

	var disableZoomDygraph = setInterval(function() {
		if(typeof Dygraph !== 'undefined') {
			Dygraph.prototype.doZoomY_ = function(lowY, highY) {return;};
			clearInterval(disableZoomDygraph);
		}
	}, 100);
});
