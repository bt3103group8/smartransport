$(document).ready(function() {
  var GM_API_KEY = 'AIzaSyDznmADRIfedTJdsrizem5IIeIWs76D5Xw';

  var addAutocomplete = setInterval(function() {
    if(typeof google !== "undefined") {
      var startAutocomplete = new google.maps.places.Autocomplete($("#view4_start-point")[0], {componentRestrictions: {country: "sg"}});
      var endAutocomplete = new google.maps.places.Autocomplete($("#view4_end-point")[0], {componentRestrictions: {country: "sg"}});
      startAutocomplete.addListener("place_changed", function() {
        window.view4_startPoint = startAutocomplete.getPlace();
      });
      endAutocomplete.addListener("place_changed", function() {
        window.view4_endPoint = endAutocomplete.getPlace();
      });
      clearInterval(addAutocomplete);
    }
  }, 100);

  $('body').on('click', '#view4_submit_button', function() {
    if(typeof window.view4_startPoint !== 'undefined' && typeof window.view4_endPoint !== 'undefined') {
      Shiny.onInputChange("startloc", window.view4_startPoint);
      Shiny.onInputChange("endloc", window.view4_endPoint);
      Shiny.onInputChange("pingSearch", Math.random());

      $('#view4_googleMapUI-legend').hide();
      $('#route-instructions-detailed').hide();
      $('#view4_googleMapUI-loading_overlay').show();
    }
    else {
      myApp.alert('Please check that you have keyed in both From and To location!', 'Input Error');
    }
  });

  $('body').on('click', '#view4_get-location-search', function() {
    if(typeof window['geolocate'] !== 'undefined') {
      $.ajax({
        url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+window['geolocate'].lat()+','+window['geolocate'].lng()+'&key='+GM_API_KEY,
        type: 'GET',
        dataType: 'json',
      })
      .done(function(response) {
        window.view4_startPoint = response.results[0];
        $('#view4_start-point').val(response.results[0].formatted_address);
      });
    }
  });

  $('body').on('polylinesDrawn', function(event, mapId) {
    setTimeout(function() {
      var polylines = window[mapId+'googlePolylinedefaultLayerId'];
      var latlngbounds = new google.maps.LatLngBounds();
      polylines.forEach(function(polyline) {
        var pointsArray = polyline.getPath().getArray();

        pointsArray.forEach(function(point) {
          latlngbounds.extend(point);
        });
      });
      window['view4_googleMapUI-mapmap'].fitBounds(latlngbounds);
    }, 500);
  });

  $('body').on('click', '#route-instructions .item-content', function(event) {
    var route = event.currentTarget;
    $(route).siblings().hide();
    $('#view4_severityChartUI-bar_plot').html('');
    Shiny.onInputChange("route_chosen", $('#route-instructions li').index(route) + 1);
    Shiny.onInputChange("pingRouteChosen", Math.random());
    $('#view4_googleMapUI-legend').show();
    $('#view4_googleMapUI-loading_overlay').show();
  });

  var positionLegend = setInterval(function() {
    if(typeof window['view4_googleMapUI-mapmap'] !== 'undefined') {
      window['view4_googleMapUI-mapmap'].controls[google.maps.ControlPosition.BOTTOM_CENTER].push(document.getElementById('view4_googleMapUI-legend'));
      var icons = {
        walking: {
          name: 'Walking',
          icon: 'green'
        },
        bus: {
          name: 'Bus',
          icon: 'red'
        },
        train: {
          name: 'Train',
          icon: 'blue'
        }
      }
      var legend = document.getElementById('view4_googleMapUI-legend');
      for (var key in icons) {
        var type = icons[key];
        var name = type.name;
        var icon = type.icon;
        var div = document.createElement('div');
        div.style = 'display: inline-block;';
        div.innerHTML = '<i class="fa fa-square" style="color: ' + icon + '"></i>' + name;
        legend.appendChild(div);
      }
      clearInterval(positionLegend);
    }
  }, 100);

});
