jQuery(document).ready(function($) {
	window.initializeGoogleMap = function(UIID) {
		var MAPID = UIID + '-mapmap';
		var MAP_MARKER_LAYERID = UIID + '-mapgoogleMarkersdefaultLayerId';

		var GetLocationControl = function(controlDiv, map) {
			// Set CSS for the control border.
			var controlUI = document.createElement('div');
			controlUI.style.backgroundColor = '#fff';
			controlUI.style.border = '2px solid #fff';
			controlUI.style.borderRadius = '3px';
			controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
			controlUI.style.cursor = 'pointer';
			controlUI.style.margin = '10px';
			controlUI.style.textAlign = 'center';
			controlUI.title = 'Go to current location';
			controlDiv.appendChild(controlUI);

			// Set CSS for the control interior.
			var controlText = document.createElement('div');
			controlText.style.padding = '5px';
			controlText.innerHTML = '<i class="fa fa-crosshairs fa-lg" style="color: #757575"></i>';
			controlUI.appendChild(controlText);

			controlUI.addEventListener('click', function() {
			if(typeof window['geolocate'] !== 'undefined') {
				map.getZoom() > 13 ? map.setZoom(13) : map.setZoom(17);
				map.setCenter(window['geolocate']);
			}
			});
		};

		var addGetLocationControl = setInterval(function() {
			if(typeof window[MAPID] !== 'undefined') {
				$(".over_map_"+UIID).show();
				window[MAPID].setOptions({
					mapTypeControl: false,
					streetViewControl: false
				});

				var getLocationControlDiv = document.createElement('div');
				var getLocationControl = new GetLocationControl(getLocationControlDiv, window[MAPID]);
				getLocationControlDiv.index = 1;
				window[MAPID].controls[google.maps.ControlPosition.TOP_LEFT].push(getLocationControlDiv);

				clearInterval(addGetLocationControl);
			}
		}, 100);

		var addFirstMarker = setInterval(function() {
			if(typeof google !== 'undefined' &&
				typeof window['currentLocationMarker_'+UIID] === 'undefined' &&
				typeof window['geolocate'] !== 'undefined' &&
				typeof window[MAPID] !== 'undefined') {
				window['currentLocationMarker_'+UIID] = new google.maps.Marker({
					position: window['geolocate'],
					map: window[MAPID],
					animation: google.maps.Animation.DROP,
					icon: {
						path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
						scale: 5,
						rotation: 0,
						fillColor: '#c0392b',
						strokeColor: '#c0392b',
						fillOpacity: 1.0
					}
				});
				$('.over_map_'+UIID).hide();
				clearInterval(addFirstMarker);
			}
		}, 100);

		$(document).on('shiny:inputchanged', function(event) {
			if(event.name == UIID + '-map_marker_click') {
				if(window['openingMarker_'+MAPID]) {
					window['openingMarker_'+MAPID].infowindow.close();
				}
				var marker_ids = window[MAP_MARKER_LAYERID].map(function(x) {return x.id;});
				var marker_id = event.value.id;
				var marker = window[MAP_MARKER_LAYERID][marker_ids.indexOf(marker_id)];
				window['openingMarker_'+MAPID] = marker;
			}
		});
	}



	window.initializeGoogleMap('view1_googleMapUI');

	var addSwiperTrafficIncident = setInterval(function() {
		if(typeof($('.swiper-trafficIncidents')[0]) !== 'undefined') {
		  clearInterval(addSwiperTrafficIncident);
		}
		var mySwiper = myApp.swiper('.swiper-trafficIncidents', {
		  pagination:'.swiper-trafficIncidents-pagination'
		});
	}, 100);

	var hideSwiperTrafficIncident = setInterval(function() {
		if($('.swiper-trafficIncidents-pagination .swiper-pagination-bullet-active').length) {
			clearInterval(hideSwiperTrafficIncident);
			$('#less-incident').hide();
			$('#incident').css('background', 'none');
			$('#incident').css('height', 'auto');
		}
	}, 100);


	$('body').on('click', '.more-button', function(event) {
		event.currentTarget.innerHTML = (event.currentTarget.innerHTML == 'Most Recent') ? 'Nearby' : 'Most Recent';
		$('#overview-traffic-incident-title')[0].innerHTML = (event.currentTarget.innerHTML == 'Most Recent') ? 'Traffic Near You' : 'Most Recent Traffic';
		$($(this).data('more')).toggle();
		$($(this).data('less')).toggle();
	});

	$('body').on('click', '.traffic-incident-card', function(event) {
		if(window['openingMarker_'+'view1_googleMapUI-mapmap']) {
			window['openingMarker_'+'view1_googleMapUI-mapmap'].infowindow.close();
		}

		var marker_id = $(this).data('markerId');
		var marker_ids = window['view1_googleMapUI-mapgoogleMarkersdefaultLayerId'].map(function(x) {return x.id;});
		var marker = window['view1_googleMapUI-mapgoogleMarkersdefaultLayerId'][marker_ids.indexOf(marker_id)];

		marker.infowindow.open(window['view1_googleMapUI-mapmap'], marker);
		window['view1_googleMapUI-mapmap'].setZoom(13);
		window['openingMarker_'+'view1_googleMapUI-mapmap'] = marker;
		$('.page-content').scrollTop(0, 500);
	});
});
