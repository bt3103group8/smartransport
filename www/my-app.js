var myApp = new Framework7({fastClicks: false});
var $$ = Dom7;
var view1 = myApp.addView('#view-1');
var view2 = myApp.addView('#view-2');
var view4 = myApp.addView('#view-4');
var view5 = myApp.addView('#view-5');

$(document).ready(function() {
	navigator.geolocation.watchPosition(function (position) {
		if(typeof google !== 'undefined') {
			window['geolocate'] = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			var latlon = {lat: window['geolocate'].lat(), lon: window['geolocate'].lng()};
			Shiny.onInputChange('current_location',
													latlon);
		}
		if(typeof window['currentLocationMarker_view1_googleMapUI'] !== 'undefined') {
			window['currentLocationMarker_view1_googleMapUI'].setPosition(window['geolocate']);
		}
		if(typeof window['currentLocationMarker_view4_googleMapUI'] !== 'undefined') {
			window['currentLocationMarker_view4_googleMapUI'].setPosition(window['geolocate']);
		}
	}, null, {enableHighAccuracy: true, timeout: Infinity, maximumAge: 0});

	if(window.DeviceOrientationEvent) {
		window.addEventListener('deviceorientation', function(eventData) {
			if(typeof window['currentLocationMarker_view1_googleMapUI'] !== 'undefined') {
				var dir = eventData.alpha;
				var icon = window['currentLocationMarker_view1_googleMapUI'].icon;
				icon.rotation = -dir;
				window['currentLocationMarker_view1_googleMapUI'].setIcon(icon);
			}

			if(typeof window['currentLocationMarker_view4_googleMapUI'] !== 'undefined') {
				var dir = eventData.alpha;
				var icon = window['currentLocationMarker_view4_googleMapUI'].icon;
				icon.rotation = -dir;
				window['currentLocationMarker_view4_googleMapUI'].setIcon(icon);
			}
		}, false);
	}

	var dom = '<div class="navbar">' +
							'<div class="navbar-inner bg-pink">' +
								'<div class="right logo-title">SmarTransport</div>' +
								'</div>' +
							'</div>' +
						'</div>';
	$('.views .view').prepend(dom);

	$('body').on('click', '.cross-origin-link', function(event) {
		event.preventDefault();
		window.open(event.currentTarget.href);
	});

	$('.need_placeholder').each(function(index, el) {
		var dataSrcId = $(el).data('srcId');
		var runOnce = function(event) {
			setTimeout(function() {
				$(el).html($(event.target).html());
			}, 200);
		};
		$('body').on('shiny:value', function(event) {
			if(event.target.id == dataSrcId) {
				runOnce(event);
				$('body').unbind('shiny:value', runOnce);
			}
		});
		$('body').on('shiny:recalculated', function(event) {
			if(event.target.id == dataSrcId) {
					$(el).html($(event.target).html());
			}
		});
	});

	$('.need_placeholder_detach').each(function(index, el) {
		var dataSrcId = $(el).data('srcId');
		$('body').on('shiny:value', function(event) {
			if(event.target.id == dataSrcId) {
				setTimeout(function() {
					var temp = $(event.target).detach();
					temp.appendTo($(el));
				}, 2000);
			}
		});
	});

});
