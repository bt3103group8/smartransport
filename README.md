** Team members: **
- Adrian Tan
- Daniel Leong
- Do Ngoc Duc
- Lucas Tang

**The key purpose of our application is to help office workers in the Central Business District area to decide the best public transport routes to leave the workplace during evening rush hour. **

Specifically, we will take a look at the City Hall station, one of the most common starting points for these commuters on their way home due to the large number of offices around that area.

This station lies in the city centre, and is both a central station as well as an interchange for both the North-South Line and the East-West Line, the longest, and arguably most important train lines in Singapore. From this station, commuters can walk via CityLink to Esplanade station, which lies on the Circle Line. Therefore, there are three choices of MRT lines around the vicinity of this MRT station. In addition, around the City Hall station, there are two large bus stops: 04167 St. Andrew�s Cath (19 bus services), and 04111 Capitol Bldg (21 bus services).

Among this myriad of choices, we aim to help commuters find the route that can minimise the travel time, as well as maximise the comfort while travelling. Currently, there are various applications that can be used to search for the shortest travel path (the most notable one being Google Maps). However, these applications only focus on minimising travel time, without considering how crowded the train or the bus is. If the train or bus gets too crowded, people will not be able to use that route. Furthermore, boarding on crowded public transport adds on to the stress that workers are already facing daily. 


**Furthermore, the secondary purpose of our application would be to inform commuters about any public transport problems (i.e. train breakdown and road incidents) as soon as possible, so that they can make informed choice of taking alternative routes. **

This will be exceptionally helpful for our scope, as North South Line and East West Line are the lines that are most prone to breaking down recently, as can be seen from the breakdowns counting from the beginning of 2017.

In addition, incidents happening on the road may slow down traffic. Hence, besides expected travel time, users would need to know whether the chosen route (especially the segments involving bus) would be affected by any nearby traffic incidents.
